FROM php:5.6-fpm

WORKDIR /var/www

# update www-data uid / home for compatibility
RUN \
    usermod -u 1000 www-data \
    && groupmod -g 1000 www-data \
    && usermod -m -d /home/www-data www-data \
    && chsh -s /bin/bash www-data \
    && cp -r /etc/skel/. /home/www-data \
    && chown -R www-data:www-data /home/www-data

# setup apt and install basic essentials
RUN \
# auto clear cache after apt runs
    echo 'DPkg::Post-Invoke {"/bin/rm -f /var/cache/apt/archives/*.deb || true";};'> /etc/apt/apt.conf.d/clean \
# disable man pages, locales, and docs
    && echo 'path-exclude=/usr/share/doc/*' > /etc/dpkg/dpkg.cfg.d/01_nodoc \
    && echo 'path-exclude=/usr/share/locale/*' >> /etc/dpkg/dpkg.cfg.d/01_nodoc \
    && echo 'path-exclude=/usr/share/man/*' >> /etc/dpkg/dpkg.cfg.d/01_nodoc \
# update packages
    && apt update \
# install base packages
    && apt install -y --no-install-recommends \
        git \
        ssh \
        vim \
# cleanup
    && rm -rf /var/lib/apt/lists/* /tmp/* || true \
    && rm -rf /usr/share/doc/ || true \ 
    && rm -rf /usr/share/locale/ || true \
    && rm -rf /usr/share/man/ || true \
    && rmdir /var/www/html || true

# configure php packages and dependencies
RUN \
# update packages
    apt update \
# install base packages
    && apt install -y --no-install-recommends \
# for bz2
        libbz2-dev \
# for gd
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng12-dev \
        php7.0-zip \
# for imagick
        libmagickwand-dev \
        imagemagick \
# for intl
        libicu-dev \
# for pdf library
        ghostscript \
# for php mail()
        ssmtp \
# for mcrypt
        libmcrypt-dev \
# for xsl
        libxslt-dev \
# configure gd package
    && docker-php-ext-configure gd \
        --with-freetype-dir=/usr/lib/x86_64-linux-gnu/ \
        --with-jpeg-dir=/usr/lib/x86_64-linux-gnu/ \
    && docker-php-ext-install \
        bz2 \
        exif \
        gd \
        intl \
        mcrypt \
        mysqli \
        pdo_mysql \
        soap \
        xsl \
        zip \
    && pecl install imagick \
    && pecl install redis-3.1.2 \
    && pecl install xdebug-2.5.1 \
    && docker-php-ext-enable \
        imagick \
        redis \
        xdebug \
# cleanup
    && rm -rf /var/lib/apt/lists/* /tmp/* || true \
    && rm -rf /usr/share/doc/ || true \ 
    && rm -rf /usr/share/locale/ || true \
    && rm -rf /usr/share/man/ || true

RUN apt-get install -y \
        libzip-dev \
        zip \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install zip
    
# install composer
RUN \
    curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && echo '\nPATH=$HOME/bin:$HOME/.composer/vendor/bin:/usr/local/sbin:/usr/local/bin:$PATH\n' >> /home/www-data/.bashrc \
    && su - www-data -c " composer global require \
        laravel/installer \
        laravel/lumen-installer"

# install node
RUN \
    su - www-data -c "curl -L https://git.io/n-install | bash -s -- -y lts" \
    && ln -s /home/www-data/n/bin/node /usr/bin/node \
    && su - www-data -c "/home/www-data/n/bin/npm install -g yarn" \
    && rm /usr/bin/node

# include aliases
RUN \
    echo '\nif [ -f "/root/.aliases" ]; then . /root/.aliases; fi\n' >> /root/.bashrc \
    && su - www-data -c "echo -e '\nif [ -f "/home/www-data/.aliases" ]; then . /home/www-data/.aliases; fi\n' >> /home/www-data/.bashrc"

COPY mailcatcher.ini /usr/local/etc/php/conf.d/zz-mailcatcher-conf.ini
COPY php.ini /usr/local/etc/php/
COPY ssmtp.conf /etc/ssmtp/
COPY fpm-www.conf /usr/local/etc/php-fpm.d/www.conf
COPY zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY xdebug.conf /usr/local/etc/php/conf.d/zz-xdebug-conf.ini
COPY aliases /root/.aliases
COPY aliases /home/www-data/.aliases

EXPOSE 9056

